import React from 'react';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom'; //Подключение
import './App.css';
import rootRoutes from './rootRoutes';
import Logo from "./images/icons8-home-40.png";
class App extends React.Component {
  state={
    counter:20
  }

  setCounter = (e) =>{
    this.setState({
      counter: e.target.value
    });
    
  }
  render(){
    const {setCounter} = this;
    const {counter} = this.state;
    return (
      <BrowserRouter>
      <div>
        <header>
        <ul className="navbar">
          <li><Link to="/"><img src={Logo}></img></Link></li>
          <li><Link to="/">Main page</Link></li>
          <li><Link to="/posts">Posts</Link></li>
          <li><Link to="/albums">Albums</Link></li>
          <li><Link to="/users">Users</Link></li>
          <li><Link to="/photos">Photos</Link> </li>
          <li><Link to={`/posts/limit/${counter}`}>Show some posts</Link><input onChange={setCounter}/></li>
          
        </ul>
        </header>
        <Switch>
        {
          rootRoutes.map((route,key) =>(
            <Route key={key}
              {...route}
            />
          ))
        }
        </Switch>
      </div>
      </BrowserRouter>
    );
  }
}

export default App;
