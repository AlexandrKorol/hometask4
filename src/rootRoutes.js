import Home from './components/Home';
import Wrapper from './components/Wrapper';
export default [
    {
        path: '/posts',
        component: Wrapper
        
    },
    {
        path: '/users',
        component: Wrapper
        
    },
    {
        path: '/albums',
        component: Wrapper
        
    },
    {
        path: '/photos',
        component: Wrapper
    },
    {
        path: '/',
        component: Home
       
    },
]