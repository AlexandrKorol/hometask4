import React from 'react';
import Comments from './Comments';
class Item extends React.Component {
  state = {
    list: [],
    commentLimiter: 30,
    loaded: false,
    clicked: false
  }
  componentDidMount() {
    const datasetting = () => {
      fetch(`https://jsonplaceholder.typicode.com/${this.props.match.params.type}/${this.props.match.params.id}`)
        .then(res => res.json())
        .then(res => {
          this.setState({
            list: res,
            loaded: true
          });

        }
        )

    }
    setTimeout(datasetting, 100);


  }
  clickHandler = () => {
    this.setState({
      clicked: true
    })

    if (this.props.match.params.type === 'posts') {
      fetch(`https://jsonplaceholder.typicode.com/'posts'/${this.props.match.params.id}/comments`)
        .then(res => res.json())
        .then(res => {

          let result = res.filter(item => {
            if (item.postId === parseInt(this.props.match.params.id))
              return item;
          })
          this.setState({
            comments: result,
            commentsLoaded: true
          });
        });
    }
    if (this.props.match.params.type === 'albums') {
      fetch(`https://jsonplaceholder.typicode.com/'albums'/${this.props.match.params.id}/comments`)
        .then(res => res.json())
        .then(res => {
          this.setState({
            comments: res,
            commentsLoaded: true
          })
        })
    }


  }
  render() {

    const { list, loaded, comments, commentsLoaded, clicked } = this.state;
    const { clickHandler } = this;
    const {type, id} = this.props.match.params;
    if (loaded) {
      return (
        <div>
          {
            type === 'posts' ?
              <div className="post">
                <h1>Post item #{id}</h1>
                <div>
                  The post's name: {list.title}
                  <p>
                    Body: {list.body}
                  </p>
                </div>
                <button onClick={clickHandler}>Show comments</button>
                <Comments comments={comments} clickHandler={clickHandler} commentsLoaded={commentsLoaded}
                  clicked={clicked} />
              </div> :
              type === 'albums' ?
                <div className="post">
                  <h1>Album #{list.id} </h1>
                  <p>Album's title: {list.title} </p>
                  <button onClick={clickHandler}>Show comments (покаже всі 500)</button>
                  <Comments comments={comments} clickHandler={clickHandler} commentsLoaded={commentsLoaded}
                    clicked={clicked} />
                </div> :
                type === 'users' ?
                  <div className="post">
                    <h1>User item #{id}</h1>
                    <p>Username: {list.username}</p>
                    <p>Name: {list.name}</p>
                    <p>E-mail: {list.email}</p>
                    <p>Living address: {list.address.city},{list.address.street}street</p>
                    <p>Phone: {list.phone}</p>
                  </div> :
                  type === 'photos' ?
                    <div>
                      <h1>Photo item #{id} </h1>
                      <p>Short description: {list.title} </p>
                      <img src={list.thumbnailUrl} alt="img"></img>
                    </div> :
                    null

          }
        </div>
      )
    }
    else {
      return <h3>Loading...</h3>
    }
  }
}


export default Item;