import React from 'react';
import {Link } from 'react-router-dom';
const Home = ()=>{
    return(
        <ul>
            <h1>Main page</h1>
        <li><Link to="/posts">Posts</Link></li>
        <li><Link to="/users">Users</Link></li>
        <li><Link to="/albums">Albums</Link></li>
        <li><Link to="/photos">Photos</Link></li>
        </ul>
        
    )
}
export default Home;