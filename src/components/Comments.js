import React from 'react';

const Comments = (props) =>{

    if(props.commentsLoaded){
        return(
            <ul>
                {
                  props.comments.map((item,index) => {
                    return <li key={index}>
                      #{item.id}
                      <p>User: <strong>{item.name}</strong></p>
                      <p>E-mail: <strong>{item.email}</strong></p>
                      <p>Said: <strong>{item.body}</strong></p>
                    </li>
                  })
                }
              </ul>
    
        )
    }
    if(props.clicked && !props.commentsLoaded){
        return <h3>Loading</h3>
    }
    else{
        return null;
    }
}
export default Comments;