import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './Home';
import List from './List';
import Item from './Item';

const ListWrapper = () => (
    
    <Switch>
        <Route path="/:type/limit/:counter" component={List}/>
        <Route path="/:type/:id" component={Item}/>
        <Route path="/:type" component={List}/>
        <Route path="/" component={Home} />
    </Switch>
)


export default ListWrapper;