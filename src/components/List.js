import React from 'react';
import { Link } from 'react-router-dom';

const Item = (props) => {
    
    const users = 'users';
    return (
        <li>
            {
                props.item.title === undefined ?
                    <Link to={`/${props.type}/${props.item.id}`}>
                        {props.item.id}){props.item.name}
                    </Link> :
                    <Link to={`/${props.type}/${props.item.id}`}>
                        {props.item.id}){props.item.title}
                    </Link>
            }
        </li>
    );
}

class List extends React.Component {
    static defaultProps = {
        counter: 20,
    }
    state = {
        list: [],
        loaded: false,
        counter: List.defaultProps.counter

    }

    componentDidMount() {
        
        let counter = this.props.match.params.counter;
        if (counter === undefined || counter === null) {
            counter = this.state.counter;
        }
        fetch(`https://jsonplaceholder.typicode.com/${this.props.match.params.type}`)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    list: res,
                    loaded: true,
                    counter: counter
                })
            });
    }
    componentDidUpdate(){
        let counter = this.props.match.params.counter;
        if (counter === undefined || counter === null) {
            counter = this.state.counter;
        }
        fetch(`https://jsonplaceholder.typicode.com/${this.props.match.params.type}`)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    list: res,
                    loaded: true,
                    counter: counter
                })
            });
    }
    clickHandler = () => {
        this.setState({
            counter: parseInt(this.state.counter) + 20
        });

    }
    render() {

        const { list, loaded, counter } = this.state;
        const { clickHandler } = this;
        const {type} = this.props.match.params;
        if (loaded) {
            return (
                <div>
                    <h1>This is part with {type}</h1>
                    <ul className="list">
                        {
                            list.map(item => {

                                if (item.id <= counter)
                                    return (
                                        <Item key={item.id} item={item} type={type}/>
                                    )  
                            })
                        }
                    </ul>
                    {
                        type === 'posts'? 
                        <button onClick={clickHandler}>Show more posts</button>:
                        null
                    }
                </div>

            )
        }
        else {
            return <h3>Loading</h3>
        }
    }
}


export default List;